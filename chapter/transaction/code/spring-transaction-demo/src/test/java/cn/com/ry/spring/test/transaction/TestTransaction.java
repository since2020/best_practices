package cn.com.ry.spring.test.transaction;

import cn.com.ry.spring.transaction.Application;
import cn.com.ry.spring.transaction.service.TransactionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class TestTransaction {
    private static final Logger logger = LoggerFactory.getLogger(TestTransaction.class);

    @Autowired
    TransactionService transactionService;

    //read-only事务  方法开始是占用链接
    @Test
    public void testFindTransactional() {
        transactionService.findTransactional();
    }

    //没有事务  执行数据库的时候才占用链接
    @Test
    public void testFind() {
        transactionService.find();
    }


    //模拟并发，打满数据连接池
    @Test
    public void testFindTransactionalConcurrency() {
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    logger.info("{}开始执行", Thread.currentThread().getName());
                    transactionService.findTransactional();
                    logger.info("{}结束执行", Thread.currentThread().getName());
                }
            }).start();
        }
    }


    //error  可回滚
    @Test
    public void testUpdateTransactionalError() {
        transactionService.updateTransactionalError("1001");
    }

    //异常捕获 不可回滚
    @Test
    public void testUpdateTransactional() {
        try {
            transactionService.updateTransactional("1003");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //异常捕获 添加回滚事件  不可回滚
    @Test
    public void testUpdateTransactionalRollback() {
        transactionService.updateTransactionalRollback("1003");
    }


    //抛出异常  不可回滚
    @Test
    public void testUpdateTransactionalException() {
        try {
            transactionService.updateTransactionalException("1004");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //抛出异常，添加rollback条件   可回滚
    @Test
    public void testUpdateTransactionalExceptionRollback() {
        try {
            transactionService.updateTransactionalExceptionRollback("1005");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //非事务  不回滚
    @Test
    public void testUpdate() {
        try {
            transactionService.update("1006");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //测试事务的传播特性
    @Test
    public void testNestTransaction() {

//        for (int i = 0; i < 7; i++) {
//        try {
//            int i = 2;
//            transactionService.updateTransactionNestd(i);
//        } catch (Exception e) {
//            logger.error("事务传播特性", e);
//        }
//        }
        transactionService.updateTransactionNestd(1);
//        transactionService.nestTransactionMandatory();
    }


    @Test
    public void testIsolationTransaction() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                transactionService.updateIsolation();
            }
        }).start();

        transactionService.getIsolation();


    }

    public void doc() {

//       事务的传播行为 Propagation
//        * 保证同一个事务中
//        Propagation.REQUIRED: 支持当前事务，如果不存在 就新建一个(默认)
//        Propagation.SUPPORTS: 支持当前事务，如果不存在，就不使用事务
//        Propagation.MANDATORY: 支持当前事务，如果不存在，抛出异常
//        * 保证没有在同一个事务中
//        Propagation.REQUIRES_NEW:  如果有事务存在，挂起当前事务，创建一个新的事务
//        Propagation.NOT_SUPPORTED: 以非事务方式运行，如果有事务存在，挂起当前事务
//        Propagation.NEVER: 以非事务方式运行，如果有事务存在，抛出异常
//        Propagation.NESTED: 如果当前事务存在，则嵌套事务执行

//        事务隔离级别 isolation
//        解决读问题: 设置事务隔离级别（5种）
//        Isolation.DEFAULT 这是一个PlatfromTransactionManager默认的隔离级别，使用数据库默认的事务隔离级别.
//        Isolation.READ_UNCOMMITTED 未提交读（read uncommited） :脏读，不可重复读，虚读都有可能发生
//        Isolation.READ_COMMITTED 已提交读 （read commited）:避免脏读。但是不可重复读和虚读有可能发生  （需事务提交）
//        Isolation.REPEATABLE_READ 可重复读 （repeatable read） :避免脏读和不可重复读.但是虚读有可能发生. （行锁）
//        Isolation.SERIALIZABLE 串行化的 （serializable） :避免以上所有读问题. （表锁）
//        Mysql 默认:可重复读
//        Oracle 默认:读已提交

//        分布式事务
//        2PC
//        3PC

//        应用
//        AT XT SAGA TCC
    }
}
