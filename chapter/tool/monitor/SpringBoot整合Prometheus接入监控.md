# SpringBoot整合Prometheus接入监控


## 1.前言：
该监控特点：
+ 代码零修改，配置简单，无改造风险
+ 看板内容丰富，包含接口监控及数据库监控
+ 对接速度快，10分钟内可看

## 2.需要工具

prometheus 
 
Kibana

## 3.接入方式

###3.1 添加jar包

```
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
<dependency>
  <groupId>io.micrometer</groupId>
  <artifactId>micrometer-registry-prometheus</artifactId>
</dependency>
```

### 3.2 配置文件中添加配置（application.properties）

```
management.endpoints.web.exposure.include=prometheus,health
management.endpoint.health.show-details=always
management.metrics.tags.application=${spring.application.name}
```

##### 3.3 验证是是否对接成功

范围监控数据接口：http://localhost:8080/actuator/prometheus,结果如下：

![](image/result.png)

### 4.Prometheus配置

##### 4.1 配置应用

> 在prometheus配置监控我们的SpringBoot应用，完整配置如下所示。
```
# my global config
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
      # - alertmanager:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  - job_name: 'prometheus'
    static_configs:
    - targets: ['127.0.0.1:9090']
###以下内容为SpringBoot应用配置
  - job_name: 'springboot_prometheus'
    scrape_interval: 5s
    metrics_path: '/actuator/prometheus'
    static_configs:
      - targets: ['127.0.0.1:8080']
``` 

##### 4.2启动Prometheus

> 启动Prometheus，浏览器访问，查看Prometheus页面，如图所示。

![](image/1.png)

>点击如图所示位置，可以查看Prometheus监控的应用。

![](image/2.png)

>也可以查看很多指数，如下所示。

![](image/3.png)

### 5.Grafana配置

>启动Grafana，配置Prometheus数据源，这里以ID是4701的Doshboard为例（地址：[https://grafana.com/dashboards/4701](https://grafana.com/dashboards/4701)）如图。

![](image/4.png)

>在Grafana内点击如图所示import按钮

![](image/5.png)

>在如图所示位置填写4701，然后点击load。

![](image/6.png)

>接下来导入Dashboard。

![](image/7.png)

>导入后就可以看到我们的SpringBoot项目对应的指标图表了，如图。

![](image/8.png)


### 其它

https://www.jianshu.com/p/d1f3ed48f367