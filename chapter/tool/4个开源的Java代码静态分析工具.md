## 1. PMD

PMD是一款采用BSD协议发布的Java程序代码检查工具。该工具可以做到检查Java代码中是否含有未使用的变量、是否含有空的抓取块、是否含有不必要的对象等。该软件功能强大，扫描效率高，是Java程序员debug的好帮手。

PMD支持的编辑器包括：
JDeveloper、 Eclipse、JEdit、JBuilder、BlueJ、CodeGuide、NetBeans/Sun Java Studio Enterprise/Creator、IntelliJ IDEA、TextPad、Maven、Ant,、Gel、JCreator和Emacs。

## 2. FindBugs

FindBugs是一个能静态分析源代码中可能会出现Bug的Eclipse插件工具。它也可以分析被编译过的程序。这个工具已经被下载了超过70万次。

## 3. JLint

Jlint 可以帮助你检查 Java 代码找出 Bug，不一致和同步问题。JLint 运行非常快速，即使你的项目非常大，它也只需要几秒钟就能检查所有的类。Jlint 已经成功的商业环境中应用。

## 4. Checkstyle

Checkstyle 是一个用来帮助开发者编写符合编码标准的代码的工具。它是高度可以配置的，几乎可以支持任何编码标准。它也可以用来帮助发现类的设计问题，重复代码，问题代码等等。这个工具可以和 Ant 整合。
