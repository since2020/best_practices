## 加锁

```
public String lockAndReturnKeyNew(String key, long timeout) {
        long orginalTimeout = timeout;
        long checkTimeout = timeout;
        String lockName = key + "_lock";

        while (checkTimeout >= 0) {
            long expires = System.currentTimeMillis() + checkTimeout + 1;
            String expiresStr = String.valueOf(expires);
            String LOCK_SUCCESS = "OK";
            String SET_IF_NOT_EXIST = "NX";
            String SET_WITH_EXPIRE_TIME = "PX";
            String result = redisInterface.set(lockName, expiresStr, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, Long.valueOf(orginalTimeout).intValue() / 1000);
            if (LOCK_SUCCESS.equals(result)) {
                return lockName;
            }

            checkTimeout -= 10;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                LOGGER.error("当前线程被强制中断，请检查.", e);
                Thread.currentThread().interrupt();

            }
        }
        return null;
    }


```

## 解锁

```
 public void unlock(String key) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("key：{}已清除锁成功", key);
        }
        redisInterface.del(key + "_lock");
    }

```
