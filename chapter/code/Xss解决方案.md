# Xss解决方案

## 添加代码

+ XssFilter.java

```
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class XssFilter implements Filter {

    /**
     * 排除链接
     */
    private List<String> excludes = new ArrayList<>();

    /**
     * 排除的参数
     */
    private List<String> excludeParams = new ArrayList<>();

    /**
     * xss过滤开关
     */
    private boolean enabled = true;

    @Override
    public void init(final FilterConfig filterConfig) {
        final String tempExcludes = filterConfig.getInitParameter("excludes");
        final String tempEnabled = filterConfig.getInitParameter("enabled");
        final String tempExcludeParams = filterConfig.getInitParameter("excludeParams");
        if (tempEnabled!=null&&!tempEnabled.trim().equals("")) {
            enabled = Boolean.valueOf(tempEnabled);
        }
        if (tempExcludes!=null&&!tempExcludes.trim().equals("")) {
            String[] url = tempExcludes.split(",");
            Collections.addAll(excludes, url);
        }
        if (tempExcludeParams!=null&&!tempExcludeParams.trim().equals("")) {
            String[] url = tempExcludeParams.split(",");
            Collections.addAll(excludeParams, url);
        }
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        if (handleExcludeURL(req)) {
            chain.doFilter(request, response);
            return;
        }
        final XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper((HttpServletRequest) request, excludeParams);
        chain.doFilter(xssRequest, response);
    }

    private boolean handleExcludeURL(final HttpServletRequest request) {
        if (!enabled) {
            return true;
        }
        if (excludes == null || excludes.isEmpty()) {
            return false;
        }
        final String url = request.getServletPath();
        for (String pattern : excludes) {
            Pattern p = Pattern.compile("^" + pattern);
            Matcher m = p.matcher(url);
            if (m.find()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {

    }
}

```

+ XssHttpServletRequestWrapper.java


```

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {
    private static final Logger log = LoggerFactory.getLogger(XssHttpServletRequestWrapper.class);
    /**
     * 排除的参数
     */
    private List<String> excludeParams = new ArrayList<>();

    // 判断是否是上传 上传忽略
    boolean isUpData = false;

    public XssHttpServletRequestWrapper(final HttpServletRequest request) {
        super(request);
    }

    public XssHttpServletRequestWrapper(final HttpServletRequest request, final List<String> excludeParams) {
        super(request);
        this.excludeParams = excludeParams;
        String contentType = request.getContentType();
        if (null != contentType) {
            isUpData = contentType.startsWith("multipart");
        }
    }

    @Override
    public String getHeader(final String header) {
        return stripXSS(super.getHeader(header), header, "getHeader");
    }

    @Override
    public Enumeration<String> getHeaders(final String header) {
        Enumeration<String> headers = super.getHeaders(header);
        List<String> values = new ArrayList<>();
        while (headers.hasMoreElements()) {
            try {
                String value = stripXSS(headers.nextElement(), header, "getHeaders");
                values.add(value);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        if (values.isEmpty()) {
            return headers;
        }
        return new XssEnumerator(0, values.size(), values);
    }

    @Override
    public Object getAttribute(final String attr) {
        Object value = super.getAttribute(attr);
        //noinspection ConditionCoveredByFurtherCondition
        if (value != null && value instanceof String) {
            String newValue = Jsoup.clean((String) value, Whitelist.relaxed()).trim();
            if (!newValue.equals((String) value)) {
                log.info("getAttribute xss字符串过滤前：" + value + "；" + "过滤后：" + newValue);
            }
            return newValue;
        }

        return value;
    }

    @Override
    public String getParameter(final String parameter) {
        return stripXSS(super.getParameter(parameter), parameter, "getParameter");
    }

    @Override
    public String[] getParameterValues(final String parameter) {
        final String[] values = super.getParameterValues(parameter);
        if (values != null) {
            final int length = values.length;
            final String[] escapseValues = new String[length];
            for (int i = 0; i < length; i++) {
                // 防xss攻击和过滤前后空格
                escapseValues[i] = stripXSS(values[i], parameter, "getParameterValues");
            }
            return escapseValues;
        }
        return values;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (isUpData) {
            return super.getInputStream();
        } else {
            //处理原request的流中的数据
            byte[] bytes = inputHandlers(super.getInputStream()).getBytes();
            final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            return new ServletInputStream() {
                @Override
                public int read() {
                    return bais.read();
                }

            };
        }
    }

    /**
     * xss过滤
     *
     * @param value     值
     * @param parameter 参数
     * @return 新值
     */
    private String stripXSS(final String value, final String parameter, final String loglabel) {
        if (getNoCheckParameter(parameter) && value != null && !value.trim().equals("")) {
            String newValue = Jsoup.clean(value, Whitelist.relaxed()).trim();
            if (!newValue.equals(value)) {
                log.info("{} xss字符串过滤前：" + value + "；" + "过滤后：" + newValue, loglabel);
            }
            return newValue;
        }
        return value;
    }

    /**
     * 判断name是否应该拦截
     *
     * @param parameter 参数名
     * @return 不拦截返回true，拦截返回false
     */
    private boolean getNoCheckParameter(final String parameter) {
        for (String parameters : excludeParams) {
            if (parameter.equals(parameters)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 对body进行xss过滤
     */
    private String inputHandlers(final ServletInputStream servletInputStream) {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(servletInputStream, StandardCharsets.UTF_8));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (servletInputStream != null) {
                try {
                    servletInputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        String value = sb.toString();
        String newValue = Jsoup.clean(value, Whitelist.relaxed()).trim();
        if (!newValue.equals(value)) {
            log.info("getRequestBody xss字符串过滤前：" + value + "；" + "过滤后：" + newValue);
        }
        return newValue;
    }

    static class XssEnumerator implements Enumeration<String> {
        int count; // 计数器
        int length; // 存储的数组的长度
        List<String> dataArray; // 存储数据数组的引用

        XssEnumerator(int count, int length, List<String> dataArray) {
            this.count = count;
            this.length = length;
            this.dataArray = dataArray;
        }

        public boolean hasMoreElements() {
            return (count < length);
        }

        public String nextElement() {
            return dataArray.get(count++);
        }

    }
}

```

+ 添加依赖 pom.xml

```

 <dependency>
    <groupId>org.jsoup</groupId>
    <artifactId>jsoup</artifactId>
    <version>1.11.3</version>
 </dependency>


```

## 使用方案

### Springboot

#### 注入配置

```

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.DispatcherType;
import java.util.HashMap;
import java.util.Map;

/**
 * xss防跨站攻击配置.
 *
 */
@Configuration
public class XssFilterConfiguration {

    /**
     * 是否开启xss
     */
    @Value("${ry.xss.enabled:true}")
    private String enabled;

    /**
     * 不进行xss拦截的url
     */
    @Value("${ry.xss.excludes:}")
    private String excludes;

    /**
     * 进行xss拦截的url
     */
    @Value("${ry.xss.urlPatterns:}")
    private String urlPatterns;

    /**
     * 不进行xss拦截的请求参数
     */
    @Value("${dorado.xss.exclude.params:}")
    private String excludeParams;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Bean
    public FilterRegistrationBean xssFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        // 过滤请求类型
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        // 过滤器类（继承Filter）
        registration.setFilter(new XssFilter());
        // 过滤url
        registration.addUrlPatterns(urlPatterns.split(","));
        // 过滤器名字
        registration.setName("xssFilter");
        // 过滤器顺序
        registration.setOrder(Integer.MAX_VALUE);
        // 排除url
        Map<String, String> initParameters = new HashMap<>();
        initParameters.put("excludes", excludes);
        initParameters.put("enabled", enabled);
        initParameters.put("excludeParams", excludeParams);
        //Filter 初始化参数
        registration.setInitParameters(initParameters);

        return registration;
    }
}


```

#### 修改配置 xx.properties

```
# 是否启用xss，默认值 true，启用
ry.xss.enabled=true


# 不进行xss拦截的url，默认值空
ry.xss.excludes=/user1,/user2


# 进行xss拦截的url， 默认拦截所有请求
ry.xss.urlPatterns=/*


# 不进行xss拦截的请求参数，默认值空
ry.xss.exclude.params=a,b

``` 

### 非SpringBoot

#### web.xml中添加配置

```

<!--放在web.xml的值前面-->
    <!--filter按文档的顺序加载-->
    <!--保证xssFilter第一个加载-->
    <filter>
        <filter-name>xssFilter</filter-name>
        <filter-class>com.ymdd.dorado.adapter.linklog.example.fsp.server.xss.XssFilter</filter-class>
        <init-param>
            <!--是否开启xss-->
            <param-name>enabled</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <!--不进行xss拦截的url-->
            <!--不进行xss拦截的url，默认值空-->
            <!--excludes=/user1,/user2-->
            <param-name>excludes</param-name>
            <param-value></param-value>
        </init-param>
        <init-param>
            <!--不进行xss拦截的请求参数-->
            <!--不进行xss拦截的请求参数，默认值空-->
            <!--excludeParams=a,b-->
            <param-name>excludeParams</param-name>
            <param-value></param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <!--进行拦截的url-->
        <filter-name>xssFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

```
