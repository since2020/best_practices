# EDS-RocketMq消费不支持多集群问题



### 代码修改



#### 老代码

![image-20210915115609256](C:\Users\uc\AppData\Roaming\Typora\typora-user-images\image-20210915115609256.png)

#### 新代码

![image-20210915120124532](C:\Users\uc\AppData\Roaming\Typora\typora-user-images\image-20210915120124532.png)



### 问题确认流程



#### 内容背景

有两个集群，分别为: 

ROCKETMQ1  对应的 nameserver=10.206.36.22:9876

ROCKETMQ2  对应的 nameserver=10.201.1.48:9876

需要在 ROKCETMQ2集群上创建一个 topic=test  cousumerGroup=test_group的消费组,ROCKETMQ1上没有topic=test consmerGroup=test_group的消费组



#### 问题判断



1.运行Engine发现，在集群ROCKETMQ1 上创建了 topic=test consmerGroup=test_group的消费组，并且显示为那台测试机连接，然后调试代码如下：

![image-20210915141902270](C:\Users\uc\AppData\Roaming\Typora\typora-user-images\image-20210915141902270.png)



consumer中显示的是连接到了ROCKETMQ2（nameserver=10.201.1.48:9876），但实际连接到的ROCKETMQ1  



2.然后将Engine中的代码单独拿出来写个demo，参数设置成一模一样，发现在单独的Demo中连接的是ROCKETMQ2，结果正确。这感觉就不太合理了。



3.由于Engine中会初始化两套集群ROCKETMQ1和ROCKETMQ2，但每次初始化的时候都是在ROCKETMQ1中创建消费组，所以这次决定想办法让Engine先初始好ROCKETMQ2中的消费组（具体如何做到就不细讲了），结果发现，在集群ROCKETMQ2 上创建了 topic=test consmerGroup=test_group的消费组，连之前在ROCKETMQ1集中中的消费组也在ROCKETMQ2上创建了。现象显示： 集群并不受consumer中的nameseserAddr的影响，而是受第一次初始化的影响。



4.目前只有调试RocketMq代码看看，一路跟踪，发现的结果如下：

```java
类 com.alibaba.rocketmq.client.impl.consumer.DefaultMQPushConsumerImpl的第 698 行
this.mQClientFactory =
MQClientManager.getInstance().getAndCreateMQClientInstance(this.defaultMQPushConsumer,
            this.rpcHook);
```



其中 defaultMQPushConsumer 中的namersrvAddr=10.201.1.48:9876(ROCKETMQ2),而转换后的mQClientFactory的对象中namersrvAddr=10.206.36.22:9876(ROCKETMQ1),问题就出在这里，如下



![image-20210915143509556](C:\Users\uc\AppData\Roaming\Typora\typora-user-images\image-20210915143509556.png)

![image-20210915143623841](C:\Users\uc\AppData\Roaming\Typora\typora-user-images\image-20210915143623841.png)



发现mQClient是从缓存中获取，key为在创建Consumer的时候设置的id,如下：

![image-20210915144225492](C:\Users\uc\AppData\Roaming\Typora\typora-user-images\image-20210915144225492.png)

该clientId=本机地址@变量 重复，当为一套集群的时候该变量重复拿出的机器连接是一样的，问题不大，但出现多集群会出现混乱。



5. 设置consumer的clientId的时候添加集群名称，并面两套集群clientId重复，如下：

![image-20210915144524793](C:\Users\uc\AppData\Roaming\Typora\typora-user-images\image-20210915144524793.png)

结果问题解决，在 ROKCETMQ2集群上创建一个 topic=test  cousumerGroup=test_group的消费组，而配置ROCKETMQ1的消费组连接在ROCKETMQ1上。

### 结论

针对RocketMQ创建consumer的时候 setInstanceName  的参数不同集群不能重复（连接信息，不涉及消费组信息），如果重复就会只用第一条。