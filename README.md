
## 最佳实践

 [LinkLog日志管理](chapter/log/doc/linklog.md)    

 [Spring事务管理](chapter/transaction/doc/springtransaction.md)


## 工具

[常用工具](chapter/tool/常用工具.md)

[SpringBoot整合Prometheus接入监控](chapter/tool/monitor/SpringBoot整合Prometheus接入监控.md)

[生成数据库文档](chapter/tool/生成数据库文档.md)

[micrometer千分尺](chapter/tool/micrometer.md)

[4 个开源的 Java 代码静态分析工具](chapter/tool/4个开源的Java代码静态分析工具.md)

[Lombok的基本使用](chapter/tool/Lombok的基本使用.md)

[springMVC中增加spring-boot-actuator](chapter/tool/springMVC中增加spring-boot-actuator.md)

## 中间件配置优化

[虚拟化造成zk网卡配置问题](chapter/middleware/zookeeper/虚拟化造成zk网卡配置问题.md)

[nginx配置](chapter/middleware/ngnix/nginx.conf)

[sentinel](chapter/middleware/sentinel/Sentinel配置持久化.md)

## JVM原理及调优

## 并发编程

## 代码块

[redis作为分布式事务锁](chapter/code/redis作为分布式事务锁.md)

[Xss解决方案](chapter/code/Xss解决方案.md)

[Base64算法](chapter/code/Base64算法.md)

[雪花算法](chapter/code/雪花算法.md)

[通用加解密算法](chapter/code/通用加解密算法.md)

[HttpClient最佳实践](chapter/code/HttpClient.md)

[纵表变树结构](chapter/code/纵表变树结构.md)

## 常见理论

[常见容错机制](chapter/book/常见容错机制.md)

[设计模式6大原则](chapter/book/设计模式6大原则.md)

## 学习

[python](chapter/learn/python-opencv.md)

[后端架构师技术图谱](chapter/learn/后端架构师技术图谱.md)

[docker使用](chapter/learn/docker使用.md)

[aspect-aop](chapter/learn/bytecode/aspect-AOP.md)

[java concurrent并发编程](chapter/learn/java-concurrent并发编程.pdf)

